﻿# Anbennar - modfifying these to have our equivalents for now

### State geography triggers

state_is_in_europe = {
	OR = {
		region = sr:region_south_lencenor
		region = sr:region_north_lencenor
		region = sr:region_western_dameshead
		region = sr:region_eastern_dameshead
		region = sr:region_northern_dameshead
		region = sr:region_the_borders 
		region = sr:region_dostanor	
		region = sr:region_dragon_coast	
		region = sr:region_alen
		region = sr:region_the_reach
		region = sr:region_gerudia
		region = sr:region_adenica	
		region = sr:region_the_marches	
		region = sr:region_castanor
		region = sr:region_businor	
	}
}

state_is_in_north_america = {
	OR = {
		region = sr:region_eordand
		region = sr:region_epednan_expanse
		region = sr:region_lower_ynn
		region = sr:region_upper_ynn
		region = sr:region_broken_sea
		region = sr:region_dalaire
	}
}

state_is_in_central_america = {
	OR = {
		region = sr:region_noruin
		region = sr:region_ruined_sea
		region = sr:region_haraf
		region = sr:region_tor_nayyi
		region = sr:region_soruin
	}
}

state_is_in_south_america = {
	OR = {
		#region = sr:region_soruin # Soriun was in both?
		region = sr:region_amadia
		region = sr:region_effelai	
		region = sr:region_lai_peninsula	
	}
}

state_is_in_americas = {
	OR = {
		state_is_in_north_america = yes
		state_is_in_central_america = yes
		state_is_in_south_america = yes
	}
}

state_is_in_africa = {
	OR = {
		region = sr:region_testoria	
	}
}

state_is_in_middle_east = {
	OR = {
		region = sr:region_testoria
	}
}

state_is_in_central_asia = {
	OR = {
		region = sr:region_testoria
	}
}

state_is_in_india = {
	OR = {
		region = sr:region_testoria	
	}
}

state_is_in_east_asia = {
	OR = {
		region = sr:region_testoria
	}
}

state_is_in_china = {
	OR = {
		region = sr:region_testoria
	}
}

state_is_in_southeast_asia = {
	OR = {
		region = sr:region_testoria
	}
}
### Country geography triggers
# exists = capital checks are necessary only because we may test these triggers and scope switch to capitals before states are initialized properly

country_is_in_china = {
	exists = capital
	capital = {
		state_is_in_china = yes
	}
}

country_is_in_europe = {
	exists = capital
	capital = {
		state_is_in_europe = yes
	}
}

country_is_in_north_america = {
	exists = capital
	capital = {
		state_is_in_north_america = yes
	}
}

country_is_in_central_america = {
	exists = capital
	capital = {
		state_is_in_central_america = yes
	}
}

country_is_in_south_america = {
	exists = capital
	capital = {
		state_is_in_south_america = yes
	}
}

country_is_in_africa = {
	exists = capital
	capital = {
		state_is_in_africa = yes
	}
}

country_is_in_middle_east = {
	exists = capital
	capital = {
		state_is_in_middle_east = yes
	}
}

country_is_in_central_asia = {
	exists = capital
	capital = {
		state_is_in_central_asia = yes
	}
}

country_is_in_india = {
	exists = capital
	capital = {
		state_is_in_india = yes
	}
}

country_is_in_east_asia = {
	exists = capital
	capital = {
		state_is_in_east_asia = yes
	}
}

country_is_in_southeast_asia = {
	exists = capital
	capital = {
		state_is_in_southeast_asia = yes
	}
}

### Climate triggers

is_arabic_farmland = {
	OR = {
		region = sr:region_testoria
	}
}

is_asian_farmland = {
	OR = {
		region = sr:region_testoria
	}
}

is_subtropic_farmland = {
	OR = {
		region = sr:region_testoria
	}
}

is_arid_region = {
	OR = {
		region = sr:region_testoria
	}
}