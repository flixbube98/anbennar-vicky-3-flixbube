﻿POPS = {
	s:STATE_KHARUNYANA_BOMDAN = {
		region_state:Y13 = {
			create_pop = {
				culture = chengrong
				size = 5000000
			}
		}
		region_state:Y12 = {
			create_pop = {
				culture = chengrong
				size = 5100000
			}
		}
		region_state:Y32 = {
			create_pop = {
				culture = chengrong
				size = 3920000
			}
		}
	}
	
	s:STATE_LOWER_TELEBEI = {
		region_state:Y32 = {
			create_pop = {
				culture = ranilau
				size = 1380000
			}
		}
	}
	
	s:STATE_BIM_LAU = {
		region_state:Y15 = {
			create_pop = {
				culture = ranilau
				size = 800000
			}
		}
		region_state:Y14 = {
			create_pop = {
				culture = ranilau
				size = 7100000
			}
		}
		region_state:Y32 = {
			create_pop = {
				culture = ranilau
				size = 160000
			}
		}
	}
	
	s:STATE_JIEZHONG = {
		region_state:R13 = {
			create_pop = {
				culture = kintonan
				size = 3190000
			}
		}
	}
	
	s:STATE_QIANZHAOLIN = {
		region_state:R13 = {
			create_pop = {
				culture = kintonan
				size = 2610000
			}
		}
	}
	
	s:STATE_WANGQIU = {
		region_state:R13 = {
			create_pop = {
				culture = kintonan
				size = 1470000
			}
		}
	}
	
	s:STATE_SIKAI = {
		region_state:Y17 = {
			create_pop = {
				culture = sikai
				size = 6640000
			}
		}
	}
	
	s:STATE_DEKPHRE = {
		region_state:Y17 = {
			create_pop = {
				culture = teplin
				size = 2230000
			}
		}
	}
	
	s:STATE_KHINDI = {
		region_state:Y32 = {
			create_pop = {
				culture = risbeko
				size = 2500000
			}
			create_pop = {
				culture = biengdi
				size = 1
			}
		}
		region_state:Y16 = {
			create_pop = {
				culture = risbeko
				size = 700000
			}
			create_pop = {
				culture = biengdi
				size = 1
			}
		}
	}
	
	s:STATE_KHABTEI_TELENI = {
		region_state:Y16 = {
			create_pop = {
				culture = biengdi
				size = 4360000
			}
		}
	}
	
	s:STATE_RONGBEK = {
		region_state:Y18 = {
			create_pop = {
				culture = risbeko
				size = 510000
			}
		}
	}
	
	s:STATE_HINPHAT = {
		region_state:Y20 = {
			create_pop = {
				culture = hinphat
				size = 3330000
			}
			create_pop = {
				culture = nephrite_dwarf
				size = 370000
			}
		}
	}
	
	s:STATE_NAGON = {
		region_state:Y19 = {
			create_pop = {
				culture = gon
				size = 900000
			}
		}
		region_state:Y32 = {
			create_pop = {
				culture = gon
				size = 700000
			}
		}
		region_state:B07 = {
			create_pop = {
				culture = gon
				size = 110000
			}
		}
	}
	
	s:STATE_KHOM_MA = {
		region_state:Y19 = {
			create_pop = {
				culture = khom
				size = 9150000
			}
		}
	}
	
	s:STATE_PHONAN = {
		region_state:Y19 = {
			create_pop = {
				culture = phonan
				size = 2450000
			}
		}
	}
	
	s:STATE_HOANGDESINH = {
		region_state:A09 = {
			create_pop = {
				culture = pinghoi
				size = 500000
			}
			create_pop = {
				culture = paru
				size = 1
			}
		}
		region_state:Y19 = {
			create_pop = {
				culture = khom
				size = 2790000
			}
			create_pop = {
				culture = pinghoi
				size = 1
			}
			create_pop = {
				culture = paru
				size = 1
			}
		}
	}
	
	s:STATE_TLAGUKIT = {
		region_state:Y22 = {
			create_pop = {
				culture = hujan
				size = 100000
			}
			create_pop = {
				culture = sirtana
				size = 40000
			}
		}
		region_state:Y24 = {
			create_pop = {
				culture = banyak
				size = 300000
			}
			create_pop = {
				culture = hujan
				size = 300000
			}
		}
	}
	
	s:STATE_SIRTAN = {
		region_state:Y22 = {
			create_pop = {
				culture = sirtana
				size = 620000
			}
		}
	}
	
	s:STATE_KUDET_KAI = {
		region_state:Y33 = {
			create_pop = {
				culture = bokai
				size = 2500000
			}
		}
		region_state:Y21 = {
			create_pop = {
				culture = bokai
				size = 1620000
			}
		}
	}
	
	s:STATE_YEMAKAIBO = {
		region_state:Y33 = {
			create_pop = {
				culture = bokai
				size = 560000
			}
		}
	}
	
	s:STATE_ARAWKELIN = {
		region_state:Y23 = {
			create_pop = {
				culture = kelino
				size = 1660000
			}
			create_pop = {
				culture = paru
				size = 1
			}
		}
	}
	
	s:STATE_REWIRANG = {
		region_state:Y23 = {
			create_pop = {
				culture = paru
				size = 30000
			}
		}
		region_state:Y25 = {
			create_pop = {
				culture = paru
				size = 200000
			}
			create_pop = {
				culture = hujan
				size = 1
			}
		}
		region_state:Y26 = {
			create_pop = {
				culture = banyak
				size = 25000
			}
		}
		region_state:Y27 = {
			create_pop = {
				culture = hujan
				size = 1
			}
			create_pop = {
				culture = banyak
				size = 25000
			}
		}
		region_state:Y28 = {
			create_pop = {
				culture = banyak
				size = 25000
			}
		}
		region_state:Y29 = {
			create_pop = {
				culture = banyak
				size = 25000
			}
		}
	}
	
	s:STATE_MESATULEK = {
		region_state:Y23 = {
			create_pop = {
				culture = paru
				size = 90000
			}
		}
		region_state:Y33 = {
			create_pop = {
				culture = paru
				size = 300000
			}
		}
		region_state:Y30 = {
			create_pop = {
				culture = hujan
				size = 100000
			}
		}
		region_state:Y31 = {
			create_pop = {
				culture = hujan
				size = 100000
			}
			create_pop = {
				culture = banyak
				size = 1
			}
		}
	}
	
	s:STATE_NON_CHIEN = {
		region_state:Y33 = {
			create_pop = {
				culture = binhrung
				size = 1040000
			}
		}
	}
	
	s:STATE_BINHRUNGHIN = {
		region_state:Y33 = {
			create_pop = {
				culture = binhrung
				size = 2440000
			}
		}
	}
	
	s:STATE_VERKAL_OZOVAR = {
		region_state:Y20 = {
			create_pop = {
				culture = nephrite_dwarf
				size = 500000
			}
		}
	}
	
	s:STATE_486 = {
		region_state:Y32 = {
			create_pop = {
				culture = pinghoi
				size = 500000
			}
		}
	}
	
	s:STATE_487 = {
		region_state:Y33 = {
			create_pop = {
				culture = pinghoi
				size = 500000
			}
			create_pop = {
				culture = gataw
				size = 1
			}
		}
	}
	
	s:STATE_488 = {
		region_state:Y34 = {
			create_pop = {
				culture = gataw
				size = 500000
			}
		}
		region_state:B07 = {
			create_pop = {
				culture = gataw
				size = 500000
			}
		}
	}
	
	s:STATE_489 = {
		region_state:A04 = {
			create_pop = {
				culture = gataw
				size = 500000
			}
		}
	}
	
	s:STATE_490 = {
		region_state:Y33 = {
			create_pop = {
				culture = gataw
				size = 500000
			}
		}
	}
}