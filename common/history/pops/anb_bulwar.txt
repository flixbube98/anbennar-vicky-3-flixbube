﻿POPS = {
	s:STATE_REUYEL = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 560000
			}
			create_pop = {
				culture = sun_elven
				size = 50000
			}
			create_pop = {
				culture = bahari_goblin
				size = 120000
			}

		}
		region_state:A09 = {
			create_pop = {
				culture = bahari
				size = 60000
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}
		}
	}
	s:STATE_CRATHANOR = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 420000
			}
			create_pop = {
				culture = bahari_goblin
				size = 60000
			}

		}
		region_state:A09 = {
			create_pop = {
				culture = bahari
				size = 140000
			}
		}
	}
	s:STATE_MEDBAHAR = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 180000
			}
			create_pop = {
				culture = sun_elven
				size = 20000
			}
			create_pop = {
				culture = bahari_goblin
				size = 120000
			}
			create_pop = {
				culture = copper_dwarf
				size = 140000
			}

		}
	}
	s:STATE_OVDAL_TUNGR = {
		region_state:F15 = {
			create_pop = {
				culture = bahari
				size = 30000
			}
			create_pop = {
				culture = copper_dwarf
				size = 480000
			}

		}
	}
	s:STATE_MEGAIROUS = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 260000
			}
			create_pop = {
				culture = sun_elven
				size = 140000
			}
			create_pop = {
				culture = bahari_goblin
				size = 140000
			}

		}
		region_state:A09 = {
			create_pop = {
				culture = bahari
				size = 20000
			}

		}
	}
	s:STATE_AQATBAHAR = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 800000
			}
			create_pop = {
				culture = sun_elven
				size = 100000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 200000
			}
			create_pop = {
				culture = bahari_goblin
				size = 900000
			}

		}
	}
	s:STATE_BAHAR_PROPER = {
		region_state:F14 = {
			create_pop = {
				culture = bahari
				size = 440000
			}
			create_pop = {
				culture = sun_elven
				size = 330000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 50000
			}
			create_pop = {
				culture = bahari_goblin
				size = 270000
			}

		}
	}
	s:STATE_DROLAS = {
		region_state:F02 = {
			create_pop = {
				culture = brasanni
				size = 260000
			}
			create_pop = {
				culture = sun_elven
				size = 90000
			}

		}
		region_state:A09 = {
			create_pop = {
				culture = brasanni
				size = 20000
			}

		}
	}
	s:STATE_KUZARAM = {
		region_state:F09 = {
			create_pop = {
				culture = bahari
				size = 390000
			}
			create_pop = {
				culture = sun_elven
				size = 20000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 50000
			}

		}
	}
	s:STATE_BRASAN = {
		region_state:F02 = {
			create_pop = {
				culture = brasanni
				size = 1930000
			}
			create_pop = {
				culture = sun_elven
				size = 480000
			}

		}
	}
	s:STATE_SAD_SUR = {
		region_state:F01 = {
			create_pop = {
				culture = brasanni
				size = 60000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 280000
			}

		}
		region_state:F02 = {
			create_pop = {
				culture = brasanni
				size = 100000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 20000
			}

		}
	}
	s:STATE_LOWER_BURANUN = {
		region_state:F01 = {
			create_pop = {
				culture = brasanni
				size = 1130000
			}
			create_pop = {
				culture = sun_elven
				size = 130000
			}

		}
	}
	s:STATE_LOWER_SURAN = {
		region_state:F02 = {
			create_pop = {
				culture = brasanni
				size = 1130000
			}
			create_pop = {
				culture = sun_elven
				size = 480000
			}

		}
	}
	s:STATE_BULWAR = {
		region_state:F01 = {
			create_pop = {
				culture = zanite
				size = 2550000
			}
			create_pop = {
				culture = sun_elven
				size = 520000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 170000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 170000
			}
			create_pop = {
				culture = royal_harimari
				size = 30000
				religion = the_jadd
			}

		}
	}
	s:STATE_KUMARKAND = {
		region_state:F01 = {
			create_pop = {
				culture = zanite
				size = 2210000
			}
			create_pop = {
				culture = sun_elven
				size = 550000
			}

		}
	}
	s:STATE_WEST_NAZA = {
		region_state:F01 = {
			create_pop = {
				culture = zanite
				size = 1410000
			}
			create_pop = {
				culture = sun_elven
				size = 280000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 50000
			}

		}
	}
	s:STATE_EAST_NAZA = {
		region_state:F01 = {
			create_pop = {
				culture = zanite
				size = 1700000
			}
			create_pop = {
				culture = sun_elven
				size = 600000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 120000
			}

		}
		region_state:F12 = {
			create_pop = {
				culture = zanite
				size = 500000
			}
			create_pop = {
				culture = sun_elven
				size = 250000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 220000
			}
		}
	}
	s:STATE_JADDANZAR = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 2210000
			}
			create_pop = {
				culture = sun_elven
				size = 1560000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 520000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 520000
			}
			create_pop = {
				culture = bahari_goblin
				size = 50000
			}
			create_pop = {
				culture = gold_dwarf
				size = 260000
			}
			create_pop = {
				culture = royal_harimari
				size = 80000
				religion = the_jadd
			}

		}
	}
	s:STATE_AVAMEZAN = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 1180000
			}
			create_pop = {
				culture = surani
				size = 190000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 240000
			}

		}
	}
	s:STATE_UPPER_SURAN = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 450000
			}
			create_pop = {
				culture = sun_elven
				size = 80000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 230000
			}

		}
	}
	s:STATE_AZKA_SUR = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 730000
			}
			create_pop = {
				culture = sun_elven
				size = 320000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 320000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 160000
			}
			create_pop = {
				culture = royal_harimari
				size = 80000
				religion = the_jadd
			}

		}
	}
	s:STATE_GARLAS_KEL = {
		region_state:F10 = {
			create_pop = {
				culture = gelkari
				size = 190000
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 70000
			}

		}
		region_state:F11 = {
			create_pop = {
				culture = gelkari
				size = 20000
			}
			create_pop = {
				culture = sun_elven
				size = 130000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 20000
			}
		}
	}
	s:STATE_HARPYLEN = {
		region_state:F13 = {
			create_pop = {
				culture = gelkari
				size = 240000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 490000
			}
			create_pop = {
				culture = bahari_goblin
				size = 80000
			}

		}
	}
	s:STATE_EAST_HARPY_HILLS = {
		region_state:F13 = {
			create_pop = {
				culture = gelkari
				size = 100000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 240000
			}

		}
	}
	s:STATE_FIRANYALEN = {
		region_state:F13 = {
			create_pop = {
				culture = gelkari
				size = 170000
			}
			create_pop = {
				culture = firanyan_harpy
				size = 520000
			}

		}
	}
	s:STATE_ARDU = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 80000
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 420000
			}

		}
	}
	s:STATE_KERUHAR = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 50000
			}
			create_pop = {
				culture = sun_elven
				size = 70000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 50000
			}

		}
	}
	s:STATE_FAR_EAST_SALAHAD = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 20000
			}
			create_pop = {
				culture = sun_elven
				size = 30000
			}

		}
	}
	s:STATE_EBBUSUBTU = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 60000
			}
			create_pop = {
				culture = sun_elven
				size = 50000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 20000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 90000
			}
			create_pop = {
				culture = gold_dwarf
				size = 10000
			}
			create_pop = {
				culture = royal_harimari
				size = 10000
				religion = the_jadd
			}
		}
	}
	s:STATE_MULEN = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 70000
			}
			create_pop = {
				culture = sun_elven
				size = 40000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 560000
			}
			create_pop = {
				culture = gold_dwarf
				size = 40000
			}

		}
	}
	s:STATE_ELAYENNA = {
		region_state:F01 = {
			create_pop = {
				culture = surani
				size = 40000
			}
			create_pop = {
				culture = sun_elven
				size = 150000
			}
			create_pop = {
				culture = sandfang_gnoll
				size = 10000
			}
			create_pop = {
				culture = siadunan_harpy
				size = 480000
			}

		}
	}
}