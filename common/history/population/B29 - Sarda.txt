POPULATION = {

	c:B29 = { #Sarda
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_very_low = yes
	}

	c:B30 = { #Ynngard
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low = yes
	}

	c:B31 = { #Veykoda
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_baseline = yes
	}

	c:B32 = { #West Tipney
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low =  yes
	}

	c:B33 = { #Corinsfield
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_middling =  yes
	}

	c:B34 = { #Beggaston
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low =  yes
	}

	c:B35 = { #New Havoral
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low =  yes
	}

	c:B36 = { #Argezvale
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low =  yes
	}

	c:B37 = { #Elathael
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low =  yes
	}

	c:B38 = { #Amacimst
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_very_low = yes
	}

	c:B39 = { #Bosancovac
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_very_low = yes
	}

	c:B41 = { #Plumstead
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_high =  yes
	}

	c:B42 = { #Tiru Moine
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low =  yes
	}

	c:B45 = { #Vels Fadhecai
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}

	c:B46 = { #Minata
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_very_low = yes
	}

	c:B47 = { #Arganjuzorn
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_very_low = yes
	}

	c:B49 = { #Dragon Dominion
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_low = yes
	}

	c:B52 = { #Drevkenuc
		effect_starting_pop_wealth_medium = yes
		effect_starting_pop_literacy_very_low = yes
	}

	c:B53 = { #Freemarches
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low = yes
	}

	c:B91 = { #Ranger Republic
		effect_starting_pop_wealth_low = yes
		effect_starting_pop_literacy_low = yes
	}
}