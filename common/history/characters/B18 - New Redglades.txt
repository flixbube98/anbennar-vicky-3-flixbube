﻿CHARACTERS = {
	c:B18 = {
		create_character = {
			first_name = "Ricain"
			last_name = sil_Wesmar
			historical = yes
			ruler = yes
			age = 45
			interest_group = ig_landowners
			ig_leader = yes
			ideology = ideology_traditionalist
			traits = {
				bigoted arrogant
			}
			culture = cu:bloodgrover
			religion = regent_court
		}
	}
}
