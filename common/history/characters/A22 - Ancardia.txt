﻿CHARACTERS = {
	c:A22 = {
		create_character = {
			first_name = "Dalyon"
			last_name = "of_Banwick"
			historical = yes
			ruler = yes
			is_general = yes
			age = 55
			interest_group = ig_armed_forces
			ideology = ideology_republican_leader
			traits = {
				direct meticulous
			}
		}
		
		create_character = {
			first_name = "Otó"
			last_name = "of_Moreced"
			historical = yes
			ig_leader = yes
			is_general = yes
			commander_rank = commander_rank_2
			age = 49
			interest_group = ig_armed_forces
			ideology = ideology_traditionalist
			traits = {
				ambitious
				grifter
				popular_commander
				experienced_offensive_planner
			}
		}
	}
}
