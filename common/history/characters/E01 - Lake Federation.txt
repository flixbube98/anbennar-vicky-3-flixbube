﻿CHARACTERS = {
	c:E01 = {
		
		create_character = {
			first_name = Halghit
			last_name = Ganbaatiin
			historical = yes
			ruler = yes
			age = 51
			interest_group = ig:ig_rural_folk
			ig_leader = yes
			ideology = ideology_radical
			traits = {
				meticulous experienced_political_operator
			}
			culture = cu:khamgunai
		}
		
		create_character = {
			first_name = Paavali
			last_name = Arjen
			historical = yes
			age = 34
			interest_group = ig:ig_landowners
			ig_leader = yes
			ideology = ideology_reformer
			traits = {
				imposing master_bureaucrat
			}
			culture = cu:urmanki
		}
		
		create_character = {
			first_name = Ilufar
			last_name = Tentenqh
			historical = yes
			female = yes
			age = 42
			interest_group = ig:ig_devout
			ig_leader = yes
			ideology = ideology_moderate
			traits = {
				honorable demagogue
			}
			culture = cu:zabatlari
		}
		
		create_character = {
			first_name = Bolgh
			last_name = Targuun
			historical = yes
			age = 36
			interest_group = ig:ig_industrialists
			ig_leader = yes
			ideology = ideology_jingoist_leader
			traits = {
				ambitious engineer
			}
			culture = cu:khamgunai
		}
		
		create_character = {
			is_general = yes
			first_name = Jaakkima
			last_name = Mergirn
			historical = yes
			female = yes
			age = 47
			interest_group = ig_armed_forces
			ig_leader = yes
			ideology = ideology_republican_leader
			#hq = 
			commander_rank = commander_rank_2
			traits = {
				direct traditionalist_commander
			}
			culture = cu:metsamic
			religion = rel:goddess_follower
		}
		
		create_character = {
			is_admiral = yes
			first_name = Hannes
			last_name = Innomaa
			historical = yes
			age = 46
			interest_group = ig_armed_forces
			ideology = ideology_republican_leader
			#hq = 
			commander_rank = commander_rank_1
			traits = {
				arrogant traditionalist_commander
			}
			culture = cu:kukatodic
			religion = rel:moitsaliusko
		}
		
		create_character = {
			first_name = Bakir
			last_name = Doshurn
			historical = yes
			age = 32
			interest_group = ig:ig_petty_bourgeoisie
			ig_leader = yes
			ideology = ideology_liberal_leader
			traits = {
				charismatic literary
			}
			culture = cu:zabatlari
		}
		
		create_character = {
			first_name = Tuya
			last_name = Namjaag
			historical = yes
			female = yes
			age = 39
			interest_group = ig:ig_intelligentsia
			ig_leader = yes
			ideology = ideology_protectionist
			traits = {
				reserved erudite
			}
			culture = cu:khamgunai
		}
		
		create_character = {
			first_name = Hannes
			last_name = Eenpala
			historical = yes
			age = 31
			interest_group = ig:ig_trade_unions
			ig_leader = yes
			ideology = ideology_liberal_leader
			traits = {
				cautious basic_political_operator
			}
			culture = cu:metsamic
		}
	}
}
