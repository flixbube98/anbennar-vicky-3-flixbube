﻿CHARACTERS = {
	c:B27 = {
		create_character = {
			first_name = Emil
			last_name = sil_Onyx
			historical = yes
			ruler = yes
			age = 45
			is_general = yes
			interest_group = ig_armed_forces
			ideology = ideology_royalist
			traits = {
				wrathful ambitious basic_offensive_planner
			}
		}
		create_character = {
			age = 51
			first_name = Denar
			last_name = sil_Arca_Cenad
			interest_group = ig_devout
			ig_leader = yes
			ideology = ideology_theocrat
			traits = {
				imperious persistent
			}
		}
	}
}
