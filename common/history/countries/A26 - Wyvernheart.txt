﻿COUNTRIES = {
	c:A26 = {
		effect_starting_technology_tier_3_tech = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_racial_segregation
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism # Not allowed protectionism due to tech
		activate_law = law_type:law_land_based_taxation
		
		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned

		activate_law = law_type:law_non_monstrous_only
		activate_law = law_type:law_dark_arts_banned
		activate_law = law_type:law_amoral_artifice_banned
		activate_law = law_type:law_mundane_production	#the verdancy made them adverse to magic at game start
	
	}
}