﻿COUNTRIES = {
	c:B18 = {
		effect_starting_technology_tier_2_tech = yes

		effect_starting_politics_conservative = yes

		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_landed_voting
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion #Ravelians are oppressed
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_per_capita_based_taxation
		# No colonial affairs
		# No police
		activate_law = law_type:law_no_schools
		# No health system
		activate_law = law_type:law_traditional_magic_encouraged
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_legacy_slavery
		
		activate_law = law_type:law_non_monstrous_only

	}
}