﻿region_east_sarhal_coast = {
	states = { STATE_EAST_SARHAL_COAST }
}
region_punctured_coast = {
	states = { STATE_PUNCTURED_COAST }
}
region_kedwali_coast = {
	states = { STATE_KEDWALI_COAST }
}
region_kedwali_gulf = {
	states = { STATE_KEDWALI_GULF }
}
region_ardimyan_sea = {
	states = { STATE_ARDIMYAN_SEA }
}
#region_insyaa_sea_1 = {  #we don't need these until Insyaa has naval hubs in all regions
#	states = { STATE_INSYAA_SEA_1 }
#}
#region_insyaa_sea_2 = { 
#	states = { STATE_INSYAA_SEA_2 }
#}
#region_barrier_islands_sea = {
#	states = { STATE_BARRIER_ISLANDS_SEA }
#}
region_themsea = {
	states = { STATE_THEMSEA }
}

region_bay_of_insyaa = {
	states = { STATE_BAY_OF_INSYAA }
}
region_insyaa_sea_3 = {
	states = { STATE_INSYAA_SEA_3 }
}
region_jadd_sea = {
	states = { STATE_JADD_SEA }
}
region_gulf_of_rahen = {
	states = { STATE_GULF_OF_RAHEN }
}
region_khom_sea = {
	states = { STATE_KHOM_SEA }
}
region_ringlet_basin = {
	states = { STATE_RINGLET_BASIN  }
}
region_phokhao_pass = {
	states = { STATE_PHOKHAO_PASS }
}
region_jellyfish_coast = {
	states = { STATE_JELLYFISH_COAST }
}
region_odheonhgu_sea = {
	states = { STATE_ODHEONGU_SEA }
}
region_widows_sea = {
	states = { STATE_WIDOWS_SEA }
}
region_hokhos_sea = {
	states = { STATE_HOKHOS_SEA }
}
region_blue_sea = {
	states = { STATE_BLUE_SEA }
}
region_kodarve_lake = {
	states = { STATE_KODARVE_LAKE }
}
region_yukelqur_lake = { 
	states = { STATE_YUKELQUR_LAKE }
}
region_oriolg_channel = {
	states = { STATE_ORIOLG_CHANNEL }
}
region_zernuuk_lake = {
	states = { STATE_ZERNUUK_LAKE }
}
