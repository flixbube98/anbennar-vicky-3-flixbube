﻿law_amoral_artifice_banned = {
	group = lawgroup_artificer_ethics
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
		
	modifier = {
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	possible_political_movements = {
		law_pragmatic_artifice
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { 
				has_journal_entry = je_government_petition
				has_variable = desired_law_var
                scope:law = var:desired_law_var
			}
			add = 750
		}
	}
}

law_pragmatic_artifice = {
	group = lawgroup_artificer_ethics
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	unlocking_laws = {
		law_traditional_magic_encouraged
		law_artifice_encouraged
		law_artifice_only
	}
	
	modifier = {
		country_influence_mult = -0.2
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	possible_political_movements = {
		law_amoral_artifice_banned
		law_amoral_artifice_embraced
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { 
				has_journal_entry = je_government_petition
				has_variable = desired_law_var
                scope:law = var:desired_law_var
			}
			add = 750
		}
	}
}

law_amoral_artifice_embraced = {
	group = lawgroup_artificer_ethics
	
	icon = "gfx/interface/icons/law_icons/consumption_based_taxation.dds"
	
	unlocking_laws = {
		law_traditional_magic_encouraged
		law_artifice_encouraged
		law_artifice_only
	}
	
	on_enact = {
		recalculate_pop_ig_support = yes
	}
	
	modifier = {
		country_influence_mult = -0.4
	}
	
	possible_political_movements = {
		law_pragmatic_artifice
		law_amoral_artifice_banned
	}
	
	ai_enact_weight_modifier = { #Petitions
		value = 0
		
		if = {
			limit = { 
				has_journal_entry = je_government_petition
				has_variable = desired_law_var
                scope:law = var:desired_law_var
			}
			add = 750
		}
	}
}