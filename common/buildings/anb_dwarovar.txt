﻿
building_serpentbloom_farm = {
	building_group = bg_serpentbloom_farms
	
	texture = "gfx/interface/icons/building_icons/wheat_farm.dds"

	city_type = farm
	levels_per_mesh = 5

	unlocking_technologies = {
		enclosure
	}

	production_method_groups = {
		pmg_base_building_serpentbloom_farm
		pmg_secondary_building_serpentbloom_farm
		pmg_harvesting_process_building_serpentbloom_farm
		pmg_ownership_land_building_serpentbloom_farm
	}

	required_construction = construction_cost_low
	
	terrain_manipulator = farmland_wheat
}

building_mushroom_farm = {
	building_group = bg_mushroom_farms
	
	texture = "gfx/interface/icons/building_icons/rye_farm.dds"

	city_type = farm
	levels_per_mesh = 5

	unlocking_technologies = {
		enclosure
	}

	production_method_groups = {
		pmg_base_building_mushroom_farm
		pmg_secondary_building_mushroom_farm
		pmg_harvesting_process_building_mushroom_farm
		pmg_ownership_land_building_mushroom_farm
	}

	required_construction = construction_cost_low
	
	terrain_manipulator = farmland_rye
}


building_cave_coral = {
	building_group = bg_cave_coral
	texture = "gfx/interface/icons/building_icons/logging_camp.dds"
	city_type = wood
	required_construction = construction_cost_low
	terrain_manipulator = forestry
	levels_per_mesh = 5

	production_method_groups = {
		pmg_base_building_cave_coral
		pmg_hardwood_cave_coral
		pmg_equipment_cave_coral
		pmg_transportation_building_cave_coral
		pmg_ownership_capital_building_cave_coral
	}
}


building_mithril_mine = {
	building_group = bg_mithril_mining
	texture = "gfx/interface/icons/building_icons/iron_mine.dds"
	city_type = mine
	levels_per_mesh = 5
	required_construction = construction_cost_medium
	terrain_manipulator = mining
	
	unlocking_technologies = {
		shaft_mining
	}

	production_method_groups = {
		pmg_mining_equipment_building_mithril_mine
		pmg_explosives_building_mithril_mine
		pmg_steam_automation_building_mithril_mine
		pmg_train_automation_building_mithril_mine
		pmg_ownership_capital_building_mithril_mine
	}
}

building_gem_mine = {
	building_group = bg_gem_mining
	texture = "gfx/interface/icons/building_icons/iron_mine.dds"
	city_type = mine
	levels_per_mesh = 5
	required_construction = construction_cost_medium
	terrain_manipulator = mining
	
	unlocking_technologies = {
		shaft_mining
	}

	production_method_groups = {
		pmg_mining_equipment_building_gem_mine
		pmg_steam_automation_building_gem_mine
		pmg_train_automation_building_gem_mine
		pmg_ownership_capital_building_gem_mine
	}
}


# building_dwarovrod = {
# 	building_group = bg_dwarovrod	
# 	texture = "gfx/interface/icons/building_icons/building_railway.dds"

# 	production_method_groups = {
# 		pmg_base_building_railway
# 		pmg_passenger_trains
# 		pmg_ownership_capital_building_railway
# 	}

# 	buildable = no
# 	downsizeable = no
# 	required_construction = construction_cost_high
	
# 	ai_value = 2000 # Railways are important
# }