﻿pmg_base_building_food_industry = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_bakery
		pm_sweeteners
		pm_baking_powder
		pm_magic_feast_in_a_box # Anbennar
	}
}

pmg_canning = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_disabled_canning
		pm_cannery
		pm_vacuum_canning
	}
}

pmg_distillery = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_disabled_distillery
		# pm_dwarven_stills # Anbennar
		pm_pot_stills
		pm_patent_stills
		pm_accelerated_fermentation_stills # Anbennar
	}
}

pmg_automation_building_food_industry = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_manual_dough_processing
		pm_automated_bakery
		pm_automata_dough_processors # Anbennar
		pm_automata_bakers # Anbennar
	}
}

pmg_ownership_capital_building_food_industry = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_food_industry
		pm_privately_owned_building_food_industry
		pm_publicly_traded_building_food_industry
		pm_government_run_building_food_industry
		pm_worker_cooperative_building_food_industry
	}
}

pmg_base_building_textile_mills = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_handsewn_clothes
		pm_dye_workshops
		pm_sewing_machines
		pm_auto_patterning_needles # Anbennar
		pm_electric_sewing_machines
		pm_pattern_transmutation #Anbennar
	}
}

pmg_luxury_building_textile_mills = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_luxury_clothes
		pm_craftsman_sewing
		pm_elastics
		pm_chameleon_clothes # Anbennar
	}
}

pmg_automation_building_textile_mills = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_traditional_looms
		pm_mechanized_looms
		pm_automatic_power_looms
		pm_automata_laborers_textile # Anbennar
		pm_automata_machinists_textile # Anbennar
	}
}

pmg_ownership_capital_building_textile_mills = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_textile_mills
		pm_privately_owned_building_textile_mills
		pm_publicly_traded_building_textile_mills
		pm_government_run_building_textile_mills
		pm_worker_cooperative_building_textile_mills
	}
}

pmg_base_building_furniture_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_handcrafted_furniture
		pm_lathe
		pm_mechanized_workshops
	}
}

pmg_luxury_building_furniture_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_luxuries
		pm_luxury_furniture
		pm_precision_tools
		pm_never_rot_furniture # Anbennar
	}
}

pmg_automation_building_furniture_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_automation_disabled
		pm_watertube_boiler_building_furniture_manufacturies
		pm_rotary_valve_engine_building_furniture_manufacturies
		pm_damestear_core_building_furniture_manufacturies #Anbennar
		pm_assembly_lines_building_furniture_manufacturies
		pm_automata_laborers_furniture
		pm_automata_machinists_furniture
	}
}

pmg_ownership_capital_building_furniture_manufacturies = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_furniture_manufacturies
		pm_privately_owned_building_furniture_manufacturies
		pm_publicly_traded_building_furniture_manufacturies
		pm_government_run_building_furniture_manufacturies
		pm_worker_cooperative_building_furniture_manufacturies
	}
}

pmg_base_building_tooling_workshops = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_crude_tools
		pm_pig_iron
		pm_steel
		pm_rubber_grips
		pm_perfect_tools # Anbennar
	}
}

pmg_apparatus_production_tooling_workshops = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_apparatus_production
		pm_brass_prosthesis
		pm_mithril_supports
		pm_servo_exo_arms
		pm_industrial_exosuits
	}
}

pmg_automation_building_tooling_workshops = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_automation_disabled
		pm_watertube_boiler_building_tooling_workshops
		pm_rotary_valve_engine_building_tooling_workshops
		pm_damestear_core_building_tooling_workshops #Anbennar
		pm_assembly_lines_building_tooling_workshops
		pm_automata_laborers_tools # Anbennar
		pm_automata_machinists_tools # Anbennar
	}
}

pmg_ownership_capital_building_tooling_workshops = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_tooling_workshops
		pm_privately_owned_building_tooling_workshops
		pm_publicly_traded_building_tooling_workshops
		pm_government_run_building_tooling_workshops
		pm_worker_cooperative_building_tooling_workshops
	}
}

pmg_base_building_glassworks = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_forest_glass
		pm_leaded_glass
		pm_crystal_glass
		pm_houseware_plastics
	}
}

pmg_luxury_building_glassworks = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_disabled_ceramics
		pm_ceramics
		pm_bone_china
		pm_self_cleaning_yana # Anbennar
	}
}

pmg_glassblowing = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_manual_glassblowing
		pm_automatic_bottle_blowers
		pm_automata_laborers_glass # Anbennar
		pm_automata_machinists_glass # Anbennar
	}
}

pmg_ownership_capital_building_glassworks = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_glassworks
		pm_privately_owned_building_glassworks
		pm_publicly_traded_building_glassworks
		pm_government_run_building_glassworks
		pm_worker_cooperative_building_glassworks
	}
}

pmg_base_building_paper_mills = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_pulp_pressing
		pm_sulfite_pulping
		pm_bleached_paper
		pm_pathway_extraction_pulping # Anbennar
	}
}

pmg_automation_building_paper_mills = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_automation_disabled
		pm_watertube_boiler_building_paper_mills
		pm_rotary_valve_engine_building_paper_mills
		pm_damestear_core_building_paper_mills #Anbennar
		pm_automata_laborers_paper # Anbennar
		pm_automata_machinists_paper # Anbennar
	}
}

pmg_ownership_capital_building_paper_mills = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_paper_mills
		pm_privately_owned_building_paper_mills
		pm_publicly_traded_building_paper_mills
		pm_government_run_building_paper_mills
		pm_worker_cooperative_building_paper_mills
	}
}

pmg_fertilizer_production = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_artificial_fertilizers
		pm_improved_fertilizer
		pm_nitrogen_fixation
	}
}

pmg_fertilizer_enhancements = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_growth_beans # Anbennar
		pm_negative_energy_essence # Anbennar
	}
}

pmg_explosives_building_chemical_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_leblanc_process
		pm_ammonia-soda_process
		pm_vacuum_evaporation
		pm_brine_electrolysis
	}
}

pmg_explosives_enhancements = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_elemental_gunpowder # Anbennar
		pm_spell_sculpted_explosives # Anbennar
	}
}

pmg_automation_building_chemical_explosives = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_automation # Anbennar
		pm_automata_laborers_chemical_explosives # Anbennar
		pm_automata_machinists_chemical_explosives # Anbennar
	}
}

pmg_ownership_capital_building_chemical_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_industry
		pm_publicly_traded_building_chemical_plants
		pm_government_run_industry
		pm_worker_cooperative_industry
	}
}

pmg_ownership_capital_building_explosives_factory = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_industry
		pm_publicly_traded_building_explosives_factory
		pm_government_run_industry
		pm_worker_cooperative_industry
	}
}

pmg_synthetic_dyes = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_dye_production
	}
}

pmg_synthetic_silk = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_artificial_fibers
		pm_rayon
	}
}

pmg_ownership_capital_building_synthetics_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_industry
		pm_publicly_traded_building_synthetic_plants
		pm_government_run_industry
		pm_worker_cooperative_industry
	}
}

pmg_steelmaking_process = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_blister_steel_process
		pm_bessemer_process
		pm_open_hearth_process
		pm_electric_arc_process
	}
}

pmg_enhancements_steel = { # Anbennar
texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_evocation_spells_steel_mills # Anbennar
		pm_perpetual_flux # Anbennar
		pm_catalytic_tearite # Anbennar
	}
}

pmg_automation_building_steel_mills = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_automation_disabled
		pm_watertube_boiler_building_steel_mills
		pm_rotary_valve_engine_building_steel_mills
		pm_damestear_core_building_steel_mills #Anbennar
		pm_automata_laborers_steel # Anbennar
		pm_automata_machinists_steel # Anbennar
	}
}

pmg_ownership_capital_building_steel_mills = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_industry
		pm_publicly_traded_building_steel_mills
		pm_government_run_industry
		pm_worker_cooperative_industry
	}
}

pmg_base_building_motor_industry = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_sparkdrive_engines # Anbennar
		pm_steam_engines
		pm_electric_engines
		pm_heavy_oil_engines # Anbennar
		pm_infernal_engines # Anbennar
	}
}

pmg_automobile_production = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_automobiles_disabled
		pm_reliquary_vehicles #Anbennar
		pm_automobile_production
	}
}

pmg_motor_enhancements = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_enhancements # Anbennar
		pm_doodad_tuning # Anbennar
		pm_portable_fire_elemental # Anbennar
		pm_sparkdrive_hybridizer # Anbennar
	}
}

pmg_automation_building_motor_industry = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_automation_disabled
		pm_watertube_boiler_building_motor_industry
		pm_rotary_valve_engine_building_motor_industry
		pm_damestear_core_building_motor_industry #Anbennar
		pm_assembly_lines_building_motor_industry
		pm_automata_laborers_motor # Anbennar
		pm_automata_machinists_motor # Anbennar
	}
}

pmg_ownership_capital_building_motor_industry = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_industry
		pm_publicly_traded_building_motor_industries
		pm_government_run_industry
		pm_worker_cooperative_industry
	}
}

pmg_base_building_shipyards = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_basic_shipbuilding
		pm_complex_shipbuilding
		pm_metal_shipbuilding
		pm_arc_welding_shipbuilding
		pm_mithril_shipbuilding # Anbennar
	}
}

pmg_military_base = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_military.dds"
	ai_selection = most_productive

	production_methods = {
		pm_military_shipbuilding_wooden
		# pm_military_shipbuilding_armorclads # Anbennar
		pm_military_shipbuilding_wooden_2
		pm_military_shipbuilding_steam
		pm_military_shipbuilding_steam_2
		pm_military_shipbuilding_mithril_leviathans # Anbennar
	}
}

pmg_automation_shipyards = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_automation # Anbennar
		pm_automata_laborers_shipyards # Anbennar
		pm_automata_machinists_shipyards # Anbennar
	}
}

pmg_ownership_capital_building_shipyards = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_shipyards
		pm_privately_owned_building_shipyards
		pm_publicly_traded_building_shipyards
		pm_government_run_building_shipyards
		pm_worker_cooperative_building_shipyards
	}
}

pmg_ownership_capital_building_military_shipyards = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_military_shipyards
		pm_privately_owned_building_military_shipyards
		pm_publicly_traded_building_military_shipyards
		pm_government_run_building_military_shipyards
		pm_worker_cooperative_building_military_shipyards
	}
}

pmg_aeroplanes = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_aeroplane_production
	}
}

pmg_tanks = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_tank_production
		pm_tank_production
	}
}

pmg_ownership_capital_building_war_machine_industry = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_industry
		pm_publicly_traded_building_war_machines_induestries
		pm_government_run_industry
		pm_worker_cooperative_industry
	}
}

pmg_telephones_category = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_sending_stones # Anbennar
		pm_telephones
	}
}

pmg_radios_category = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	production_methods = {
		pm_no_radios
		pm_radios
		pm_t_wave_transceivers # Anbennar
	}
}

pmg_automation_electric = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_no_automation # Anbennar
		pm_automata_laborers_electric # Anbennar
		pm_automata_machinists_electric # Anbennar
	}
}

pmg_ownership_capital_building_electrics_industry = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_industry
		pm_publicly_traded_building_electrics_industries
		pm_government_run_industry
		pm_worker_cooperative_industry
	}
}

pmg_firearms_manufacturing = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_muskets
		pm_sparkdrive_rifles # Anbennar
		pm_rifles
		pm_repeaters
		pm_bolt_action_rifles
	}
}

pmg_automation_building_arms_industry = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_automation.dds"
	production_methods = {
		pm_automation_disabled
		pm_rotary_valve_engine_building_arms_industry
		pm_assembly_lines_building_arms_industry
		pm_automata_laborers_arms # Anbennar
		pm_automata_machinists_arms # Anbennar
	}
}

pmg_foundries = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_refining.dds"
	ai_selection = most_productive

	production_methods = {
		pm_cannons
		pm_smoothbores
		pm_breech_loaders
		pm_kinetic_absorption_barrels # Anbennar
		pm_recoiled_barrels
		pm_reactor_artillery # Anbennar
	}
}

pmg_ownership_capital_building_arms_industry = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_arms_industry
		pm_privately_owned_building_arms_industry
		pm_publicly_traded_building_arms_industry
		pm_government_run_building_arms_industry
		pm_worker_cooperative_building_arms_industry
	}
}

pmg_ownership_capital_building_artillery_foundries = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_merchant_guilds_building_artillery_foundries
		pm_privately_owned_building_artillery_foundries
		pm_publicly_traded_building_artillery_foundries
		pm_government_run_building_arms_industry
		pm_worker_cooperative_building_artillery_foundries
	}
}

pmg_base_building_munition_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_percussion_caps
		#pm_black_damestear_fragmentation # Anbennar
		pm_explosive_shells
		pm_vorpal_bullets # Anbennar
	}
}

pmg_automation_building_munition_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_automation_disabled
		pm_rotary_valve_engine_building_munition_plants
		pm_assembly_lines_building_munition_plants
		pm_automata_laborers_munitions # Anbennar
		pm_automata_machinists_munitions # Anbennar
	}
}

pmg_ownership_capital_building_munition_plants = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_ownership.dds"
	production_methods = {
		pm_privately_owned_industry
		pm_publicly_traded_building_munition_plants
		pm_government_run_industry
		pm_worker_cooperative_industry
	}
}
