﻿je_industrialization_of_the_league = {
	icon = "gfx/interface/icons/event_icons/event_trade.dds"

	scripted_button = nl_focus_gawed_button
	scripted_button = nl_integrate_gawed_button
	scripted_button = nl_focus_the_reach_button
	scripted_button = nl_integrate_the_reach_button
	scripted_button = nl_focus_gerudia_button
	scripted_button = nl_integrate_gerudia_button

	complete = {
		custom_tooltip = {
			text = indsutrialized_the_league_tt
			scope:journal_entry = {
				is_goal_complete = yes
			}
			has_variable = nl_urbanized_gerudia
			has_variable = nl_urbanized_the_reach
			has_variable = nl_urbanized_gawed
		}
		cu:dalr = {
			NOT = { culture_is_discriminated_in = ROOT }
		}
		cu:blue_reachman = {
			NOT = { culture_is_discriminated_in = ROOT }
		}
		cu:gawedi = {
			NOT = { culture_is_discriminated_in = ROOT }
		}
	}
	
	on_complete = {
		trigger_event = {
			id = urbanisation_of_nl.1
		}
	}
	
	invalid = {
		NOT = {
			any_scope_state = {
				state_region = {
					is_homeland = cu:dalr
				}
			}
			any_scope_state = {
				state_region = {
					is_homeland = cu:blue_reachman
				}
			}
			any_scope_state = {
				state_region = {
					is_homeland = cu:gawedi
				}
			}
		}
	}

	fail = {
		OR = {
			cu:dalr = {
				culture_secession_progress = { target = ROOT value > 75 }
			}
			cu:blue_reachman = {
				culture_secession_progress = { target = ROOT value > 75 }
			}
			cu:gawedi = {
				culture_secession_progress = { target = ROOT value > 75 }
			}
		}
	}
	
	current_value = {
		value = owner.var:nl_construction_var
	}
	
	on_weekly_pulse = {
		events = {
			urbanisation_of_nl.2 # focus region complete
		}
	}

	goal_add_value = {
		add = 100
	}

	progressbar = yes
	
	progress_desc = je_industrialization_of_the_league_progress

	should_be_pinned_by_default = yes

	weight = 100
	
	can_revolution_inherit = yes
}
