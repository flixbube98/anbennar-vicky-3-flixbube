﻿STATE_STEEL_BAY = {
    id = 526
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0EE4CF" "x0F9D89" "x17F5A9" "x2815EC" "x2BF5FB" "x2DA13A" "x3F0E92" "x412250" "x425ABF" "x433E80" "x4346C0" "x444E40" "x46174B" "x513040" "x605EFB" "x60A6ED" "x627F2F" "x6BF6A4" "x70C845" "x786BE0" "x7D50B4" "x8FD6DA" "x901B46" "x992011" "x9C6D63" "xAB563E" "xC42B3A" "xC4F634" "xE299B8" "xF32E4C" "xFD5F81" "xFF3191" }
    traits = { "state_trait_natural_harbors" }
    city = "x4346C0" #Ricardsport








    port = "xF32E4C" #New Telgeir








    farm = "xE299B8" #Rionfort








    mine = "x425ABF" #Wretchedcliff








    wood = "x444E40" #Bayfield








    arable_land = 98
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 10
        bg_logging = 12
        bg_iron_mining = 24
        bg_coal_mining = 32
    #1 cobalt-zinc deposit
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 4
    }
    naval_exit_id = 3125 #Matni Sea








}

STATE_GREENHILL = {
    id = 527
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0154A8" "x051F32" "x093195" "x0F86EC" "x15F0DA" "x261B47" "x29F7BB" "x2AA5F6" "x414E54" "x45CD32" "x4BF04A" "x4C7D74" "x4FFE70" "x534C6C" "x5402A3" "x707EE9" "x789118" "x7D8E7E" "x819C8C" "x96D701" "xA398DC" "xA4A4ED" "xA4E3FE" "xA9F85A" "xADC571" "xB186EE" "xCBB74B" "xE58BB3" "xE66D55" "xFFD800" }
    traits = {}
    city = "x4BF04A" #Tristanton








    port = "x45CD32" #Ruinharbour








    farm = "x051F32" #Varland








    mine = "xA9F85A" #Naraine








    wood = "xFFD800" #Erlanswood








    arable_land = 42
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 10
        bg_logging = 14
        bg_iron_mining = 24
        bg_coal_mining = 10
    #2 cobalt-zinc deposits
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 4
    }
    naval_exit_id = 3124 #Lovers Pass








}

STATE_RYAIL = {
    id = 528
    subsistence_building = "building_subsistence_farms"
    provinces = { "x5AB6B0" "x91CD55" "x952121" "x9E9A42" "xD4E2CF" "xD98345" "xE27ECD" "xF508EB" }
    traits = {}
    city = "x9E9A42" #Random








    port = "x952121" #Random








    farm = "xF508EB" #Random








    wood = "x5AB6B0" #Random








    arable_land = 18
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 11
        bg_logging = 5
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 2
    }
    naval_exit_id = 3125 #Matni Sea








}

STATE_SCATTERED_ISLANDS = {
    id = 529
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0024FF" "x15BB4F" "x2D0E85" "x402040" "x538E1F" "x8DFDBD" "x9E3E3E" "xE4547A" "xEF0BBF" "xF9F7D6" }
    traits = {}
    city = "x402040" #Random








    port = "x0024FF" #Random








    farm = "x2d0e85" #Random








    wood = "x8dfdbd" #Random








    arable_land = 10
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 4
        bg_whaling = 2
    }
    naval_exit_id = 3125 #Matni Sea








}

STATE_BRISTAILEAN = {
    id = 531
    subsistence_building = "building_subsistence_farms"
    provinces = { "x039447" "x0D4483" "x2BB631" "x409145" "x412EB0" "x4134E0" "x4FF0CD" "x8052D4" "x993939" "xAC4BA1" }
    traits = {}
    city = "x4ff0cd" #Random








    port = "x993939" #Random








    farm = "x2bb631" #Random








    wood = "x8052d4" #Random








    arable_land = 1
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    naval_exit_id = 3116 #Torn Sea








}

STATE_GUARDIAN_ISLANDS = {
    id = 532
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0B902B" "x20610D" "x233FBF" "x303ED0" "x303ED0" "x3675E0" "x435470" "x4B7937" "x562E85" "x583D16" "x59F52B" "x5AD1CB" "x606EBF" "x612C60" "x68A4C9" "x77CE38" "x797F04" "x79CAD0" "x7EF6ED" "x836423" "x873B3E" "x89DF1A" "x8C099A" "x902525" "x94775A" "x959BC9" "xA19252" "xAB4F13" "xB9630A" "xBB5181" "xC1B876" "xC26FFC" "xCE1A2B" "xD4E2D0" "xDAD8E2" "xDF69C5" "xE9D85C" "xF56177" }
    traits = {}
    city = "x59f52b" #Random








    port = "x583d16" #Random








    farm = "x902525" #Random








    mine = "xe9d85c" #New Place








    wood = "x836423" #Random








    arable_land = 1
    arable_resources = { bg_rice_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_iron_mining = 9
        bg_lead_mining = 8
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 2
    }
    naval_exit_id = 3116 #Torn Sea








}

STATE_TAMETER = {
    id = 538
    subsistence_building = "building_subsistence_farms"
    provinces = { "x18D607" "x387FCF" "x48E7BF" "x4E9BBF" "x67767A" "x8CF429" "xA151C9" "xAEDF7A" "xB0F65B" "xB92D20" "xC37B79" "xCADAFF" "xD23535" "xD7BD1A" "xDE5E5E" }
    traits = {}
    city = "xd2f3c2" #Random








    farm = "xdeac95" #Random








    wood = "xd2ad4c" #Random








    arable_land = 50
    arable_resources = { bg_maize_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_iron_mining = 36
        bg_coal_mining = 1
    }
    naval_exit_id = 3124 #Lovers Pass








}

STATE_MINAR_YOLLI = {
    id = 533
    subsistence_building = "building_subsistence_farms"
    provinces = { "x35D7D9" "x4389E1" "x749177" "x925151" "xD43535" "xE7ABE7" "x3E101E" "x48E1C0" "xBB9AD4" "xE755AB" "xEABD5D" }
    traits = {}
    city = "x749177" #Random








    port = "xe7abe7" #Random








    farm = "x48e7bf" #Random








    mine = "x387fcf" #New Place








    wood = "xcadaff" #Random








    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_iron_mining = 18
        bg_lead_mining = 30
    #1 bauxite deposit, 1 cobalt-zinc deposit
    }
    resource = {
        type = "bg_gold_fields"
        depleted_type = "bg_gold_mining"
        undiscovered_amount = 4
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 6
    }
    naval_exit_id = 3124 #Lovers Pass








}

STATE_OZTENCOST = {
    id = 536
    subsistence_building = "building_subsistence_farms"
    provinces = { "x60E5ED" "x6784E0" "x689EF0" "x6994A0" "x69A250" "x7BB548" "x8E7A4F" "x9AC029" "xD7BF1A" "xE9B493" }
    traits = {}
    city = "x7bb548" #Random








    port = "xd7bf1a" #Random 








    farm = "x69a250" #Random








    mine = "x6784e0" #New place








    wood = "x6994a0" #Random








    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_lead_mining = 8
        bg_coal_mining = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 12
    }
    naval_exit_id = 3116 #Torn Sea








}

STATE_TLACHIBAR = {
    id = 537
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x059F5E" "x1E703F" "x3230E6" "x4389DE" "x48E6BF" "x4D869B" "x4F8499" "x54D127" "x688840" "x71A0CD" "x754B6E" "x94888A" "xB9072A" "xBAEF16" "xC1CF2C" "xE5F901" }
    traits = {}
    city = "xb92d20" #Random








    farm = "xde5e5e" #Random








    wood = "xc37b79" #Random








    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_logging = 1
        bg_coal_mining = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 7
    }
}

STATE_AUVUL_TADIH = {
    id = 539
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x10D98F" "x1E4E7C" "x387E90" "x3A542B" "x44EB96" "x49514E" "x5676F0" "x5F65DE" "x646290" "x6698A2" "x6797A2" "xA0816F" "xA21D84" "xA8D5EA" "xB1AAE7" "xB6D96A" "xC32B73" "xD6D15C" "xDDCE1D" "xE04A4B" "xE0804B" "xE0B64B" "xE30A42" "xEAB6C3" "xFA33DC" }
    traits = { state_trait_harafe_desert }
    city = "x6797a2" #Random








    farm = "xa8d5ea" #Random








    mine = "xe30a42" #New Place








    wood = "xe0b64b" #Random








    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 9
        bg_lead_mining = 8
    #1 rare earths deposit
    }
}

STATE_NANI_NOLIHE = {
    id = 540
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x01B54E" "x1AE532" "x538E1E" "x5D3D51" "x5E029F" "x6356F0" "x645A50" "x6AACA0" "x73D28F" "x7BB280" "x7BB87F" "xA56F3F" "xBA596D" "xBA7ECF" "xBE03D9" "xC18584" "xCBB064" "xCBE6B2" "xDD6BA8" "xE04ABF" }
    traits = { state_trait_harafroy state_trait_harafe_desert }
    city = "x645a50" #Random








    farm = "x6356f0" #Random








    mine = "x1ae532" #New Place








    wood = "x5d3d51" #Random








    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_iron_mining = 9
        bg_lead_mining = 8
    #1 cobalt-zinc deposit
    }
}

STATE_MANGROVY_COAST = {
    id = 542
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0010FF" "x0012FF" "x29FDCF" "x44BCE0" "x62E1C9" "x8E6308" "x927129" "x977BF7" "x97AFCD" "x98C02C" "x9AC02C" "xC7269C" "xC7DF66" "xDBE07C" "xE40E1B" "xEB4799" }
    traits = { state_trait_harafroy }
    city = "x977BF7" #Random








    port = "x97AFCD" #Random








    farm = "xEB4799" #Random








    wood = "xC7DF66" #Random








    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_sugar_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 6
    }
    naval_exit_id = 3116 #Torn Sea








}

STATE_NIDI_BIKEHA = {
    id = 543
    subsistence_building = "building_subsistence_orchards"
    provinces = { "x0BD7B1" "x1A08A5" "x1A0BA7" "x330AFA" "x3A066D" "x4CD582" "x6CCEF0" "x735250" "x7AB881" "x7B2528" "x902323" "xA4BD38" "xB3E66C" "xC4C2F0" "xC8249B" "xCA849F" "xE60E1B" }
    traits = { state_trait_harafroy }
    city = "x1a08a5" #Random








    farm = "x7ab881" #Random








    mine = "xca849f" #New Place








    wood = "xc8249b" #Random








    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_logging = 1
        bg_coal_mining = 1
    }
}

STATE_ZURZUMEXIA = {
    id = 545
    subsistence_building = "building_subsistence_farms"
    provinces = { "x00C2FF" "x147220" "x1AE530" "x208CDD" "x317A7D" "x63E1C9" "x6FF4A0" "x702040" "x71FBFB" "x723840" "x727337" "x7466F0" "x848CDD" "xA53899" "xCD5552" "xDB6C09" "xE08CDD" "xE67571" }
    traits = {}
    city = "xe08cdd" #Random








    farm = "x1ae530" #Random








    port = "x848cdd" #Random








    mine = "x727337" #New Place








    wood = "x317a7d" #Random








    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        bg_lead_mining = 8
        bg_coal_mining = 1
    }
    naval_exit_id = 3116 #Torn Sea








}

STATE_MESTIKARDU = {
    id = 547
    subsistence_building = "building_subsistence_farms"
    provinces = { "x6FFEF0" "x51066D" "x8E5086" "xE8826A" "x118A5E" "x25FDCF" "x29963D" "x38ED3F" "x405A45" "x523EB0" "x6F3383" "x7A3B01" "x7C3410" "x962A2A" "x976D8B" "xA1A5E1" "xAFCB42" "xB326D4" "xBE3535" "xC100F1" "xC7249B" "xCD5652" "xCDD141" "xE3DA61" "xED2DB8" "xED994F" }
    traits = {}
    city = "xed994f" #Random








    farm = "xe3da61" #Random








    port = "x118a5e" #Random








    wood = "x976d8b" #Random








    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 5
    }
    naval_exit_id = 3120 #Artificers Coast








}


STATE_E_E_KIDILE = {
    id = 551
    subsistence_building = "building_subsistence_farms"
    provinces = { "x051790" "x0DE70B" "x293FD0" "x2AE755" "x2AFDCF" "x57E690" "x6015F9" "x6BB4E0" "x725250" "x767EF0" "x7885BB" "x799840" "x7A2628" "x7AAEF0" "x7B2628" "x7B7143" "x808B9A" "x8375B9" "x8F0CEB" "x927372" "xAAB9D4" "xB11019" "xB8588E" "xC6249B" "xCDD142" "xDA37AE" "xFAA195" }
    traits = { state_trait_harafroy }
    city = "x57e690" #Random








    farm = "xaab9d4" #Random








    port = "x6015F9" #Random








    mine = "x927372" #New Place








    wood = "x293fd0" #Random








    arable_land = 1
    arable_resources = { bg_wheat_farms bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
        v = 9
    }
    naval_exit_id = 3121
}

STATE_GOOVRAZ = {
    id = 553
    subsistence_building = "building_subsistence_farms"
    provinces = { "x0146AD" "x19E532" "x20438A" "x214295" "x22BB94" "x3BFC3E" "x415A45" "x44BDE0" "x456640" "x547339" "x615A03" "x73D9B6" "x7778C0" "x778A90" "x793636" "x7BB880" "x7CD290" "x8D7ED7" "x8E6408" "x943750" "x9B87F7" "xB93333" "xB9C527" "xBCCC4A" "xBD03D9" "xC4BB15" "xD9D8E2" "xE1303F" "xE34832" "xF933DC" "xFFA16B" }
    traits = { state_trait_harafroy }
    city = "x793636" #Random








    farm = "xB9C527" #Random








    mine = "x214295" #New Place








    wood = "x7CD290" #Random








    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_logging = 1
        bg_lead_mining = 8
        bg_coal_mining = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 13
    }
}

STATE_GOMMIOCHAND = {
    id = 554
    subsistence_building = "building_subsistence_farms"
    provinces = { "x000EC0" "x28FDCF" "x156DBE" "x17E176" "x219B09" "x429CBA" "x518E42" "x545080" "x6A2231" "x6BDA6F" "x911818" "x94F0FE" "x9A34B3" "xA3CD40" "xC5E1BF" "xCBAD63" "xE0D5B2" "xEDF132" "xEEFE94" }
    traits = { state_trait_harafroy }
    city = "x545080" #Random








    farm = "x219B09" #Random








    port = "xC5E1BF" #Random








    wood = "x6BDA6F" #Random








    arable_land = 1
    arable_resources = { bg_maize_farms bg_livestock_ranches bg_banana_plantations bg_coffee_plantations bg_cotton_plantations bg_dye_plantations bg_tobacco_plantations }
    capped_resources = {
        bg_fishing = 1
        bg_logging = 1
    }
    resource = {
        type = "bg_rubber"
        undiscovered_amount = 5
    }
    naval_exit_id = 3120 #Artificers Coast








}

STATE_WORMWAY = {
    id = 556
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x330EB5" "x4C5020" "x633AE3" "xD0E711" "xDAAE25" "xFB0023" }
    impassable = { "x4C5020" "xdaae25" }
    traits = { state_trait_harafe_desert }
    city = "x4C5020" #Desert








    farm = "x1FC47B" #NEW PLACE








    mine = "x330EB5" #NEW PLACE








    port = "xd0e711" #Fospont








    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
        bg_fishing = 1
        bg_lead_mining = 24
    #5 rare earths deposit, 1 diamonds deposit
    }
    naval_exit_id = 3121
}
STATE_KITSIL_KINN = {
    #middle Haraf desert state, where Harathil's ruins are
    id = 555
    subsistence_building = "building_subsistence_pastures"
    provinces = { "x1FC47B" }
    traits = { state_trait_harafe_desert }
    mine = "x1FC47B" #NEW PLACE








    port = "x1FC47B" #Fospont








    farm = "x1FC47B" #NEW PLACE








    city = "x1FC47B" #Desert








    arable_land = 1
    arable_resources = { bg_livestock_ranches }
    capped_resources = {
    #oil, relics and stuff
    }
}
